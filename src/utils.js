module.exports = {
    getWebsitesDosUsuarios(usuarios) {
        let websites = usuarios.map((usuario) => usuario.website);
        return websites;
    },
    formatarUsuario(usuario) {
        let { name, email, company } = usuario;
        return { name, email, companyName: company.name };
    },
    getUsuariosASC(usuarios) {
        let usuariosFormatados = usuarios.map(usuario => this.formatarUsuario(usuario));
        usuariosFormatados.sort((usuarioA, usuarioB) => {
            return usuarioA.name < usuarioB.name ? -1 :
                usuarioA.name > usuarioB.name ? 1 : 0;
        })
        return usuariosFormatados;
    },
    getSuiteNoEnderecoDoUsuario(usuarios) {
        let resultado = usuarios.filter(usuario => usuario.address.suite.match(/suite/i));
        return resultado;
    }
}