const winston = require("winston"),
    { timestamp } = winston.format,
    logger = winston.createLogger({
        format: winston.format.combine(
            timestamp({
                format: 'YYYY-MM-DD HH:mm:ss'
            }),
            winston.format.simple()
        ),
        levels: winston.config.syslog.levels,
        transports: [
            new winston.transports.File({
                filename: 'logs.log',
                level: 'info'
            })
        ]
    });

module.exports.logger = logger;
