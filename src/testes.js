const expect = require("chai").expect,
    utils = require("./utils"),
    getUsuarios = require("./servico").getUsuarios;

describe('getUsuarios', () => {

    it("Deve conter 10 usuarios", done => {

        getUsuarios()
            .then(usuarios => {
                expect(usuarios.length).to.equal(10);
                done();
            })
            .catch(erro => done(erro));

    });

});

describe("getWebsitesDosUsuarios", () => {

    it("Deve retornar um array com tamanho 10", done => {

        getUsuarios()
            .then(usuarios => {
                websites = utils.getWebsitesDosUsuarios(usuarios);
                expect(websites.length).to.equal(10);
                expect(websites).to.be.an("array");
                done();
            })
            .catch(erro => done(erro))

    });

});

describe("getUsuariosASC", () => {

    it("Deve retornar uma lista de usuarios em ordem alfabética e formatado com name, email e companyName",
        done => {

            getUsuarios()
                .then(usuarios => {
                    let usuariosASC = utils.getUsuariosASC(usuarios);
                    expect(usuariosASC.length).to.equal(10);
                    usuariosASC.forEach((usuario, i) => {
                        expect(
                            i > 0 ?
                                usuariosASC[i - 1].name < usuariosASC[i].name :
                                true
                        ).to.be.true;
                        expect(usuario).to.have.property('name');
                        expect(usuario).to.have.property('email');
                        expect(usuario).to.have.property('companyName');
                    });
                    done();
                })
                .catch(erro => done(erro));
                
        });

});

describe("getSuiteNoEnderecoDoUsuario", () => {

    it("Deve retornar um array com tamanho 7 e conter '''suite''' no endereço", done => {

        getUsuarios()
            .then(usuarios => {
                let usuariosComSuite = utils.getSuiteNoEnderecoDoUsuario(usuarios);
                expect(usuariosComSuite.length).to.equal(7);
                usuariosComSuite.forEach(usuario => {
                    expect(!!usuario.address.suite.match(/suite/i)).to.be.true;
                });
                done();
            })
            .catch(erro => done(erro))

    });

});


