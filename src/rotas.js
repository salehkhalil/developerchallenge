const app = require("express")(),
    getUsuarios = require("./servico").getUsuarios,
    utils = require("./utils"),
    asyncHandler = require('express-async-handler'),
    logger = require("./logger").logger;

app.get("/websites", asyncHandler(async (req, res) => {
    try {
        let usuarios = await getUsuarios(),
            websites = utils.getWebsitesDosUsuarios(usuarios);
        logger.info("Acessou websites.");

        res.status(200).json({
            websites: websites
        })
    } catch (erro) {
        res.status(500).json({ erro });
        logger.console.error(`Erro ao acessar websites: ${erro}`);
    }
}));

app.get("/usuarios/ASC", asyncHandler(async (req, res) => {
    try {
        let usuarios = await getUsuarios(),
            usuariosASC = utils.getUsuariosASC(usuarios);
        logger.info("Acessou usuarios em ordem alfabética.");

        res.status(200).json({
            usuarios: usuariosASC
        })
    } catch (erro) {
        res.status(500).json({ erro });
        logger.console.error(`Erro ao acessar usuarios em ordem alfabética: ${erro}`);
    }
}));

app.get("/usuarios/suite", asyncHandler(async (req, res) => {
    try {
        let usuarios = await getUsuarios(),
            usuariosSuite = utils.getSuiteNoEnderecoDoUsuario(usuarios)
        logger.info("Acessou usuários que contém suite no endereço.");

        res.status(200).json({
            usuarios: usuariosSuite
        })
    } catch (erro) {
        res.status(500).json({ erro });
        logger.console.error(`Erro ao acessar usuários que contém suite no endereço: ${erro}`);
    }
}));

app.use((req, res) => {
    res.status(404).send(
        `
        <div style="display:flex; justify-content: center; align-items: center;">
            <img 
            src='http://www.onlineinformaticacursos.com.br/images/404.png' 
            alt='404'
            >
        </div>
        `
    );
});

module.exports = app;