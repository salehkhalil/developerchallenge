const https = require('https');

exports.getUsuarios = () => {
    return new Promise((resolve, reject) => {
        https.get("https://jsonplaceholder.typicode.com/users", resposta => {

            let usuariosStringfy = '';

            resposta.on('data', buffer => usuariosStringfy += buffer);

            resposta.on('end', () => {
                resolve(JSON.parse(usuariosStringfy));
            });

        }).on("error", err => reject(err.message));
    })
}