FROM node:11.0

RUN useradd --user-group --create-home --shell /bin/false app &&\
    npm install --global npm@6.4.1

ENV HOME=home/saleh/Desktop

COPY package.json npm-shrinkwrap.json $HOME/developerChallenge
RUN chown -R app:app $HOME/*

USER root
WORKDIR $HOME/developerChallenge
RUN npm cache clean --force && npm install --silent --progress=false

USER root
COPY . $HOME/developerChallenge
RUN chown -R app:app $HOME/*
USER app

CMD ["npm", "start"]