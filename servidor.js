const http = require("http"),
    rotas = require("./src/rotas"),
    portaServidor = 8080;

http
    .createServer(rotas)
    .listen(
        portaServidor,
        () => console.log(`Servidor executado na porta ${portaServidor}`)
    );