# Developper Challenge

> ## Descrição

Aplicação back-end desenvolvida em NodeJS que carrega os dados da URL https://jsonplaceholder.typicode.com/users e trazendo as seguintes informações: 

1. Os websites de todos os usuários;
2. Usuarios em ordem alfabética apenas com as informações: nome, email e a empresa em que trabalha;
3. Todos os usuários que no endereço contém a palavra 'suite';

> ## Instalação

Clone o repositório localmente em sua máquina e dentro da pasta do projeto, execute o comando **npm install**

> ## Execução

Após a instalação, execute o comando **npm start** dentro da pasta do projeto para iniciar a aplicação

> ## Rotas

**http://localhost:8080/websites** - Traz os websites de todos os usuários

**http://localhost:8080/usuarios/asc** - Traz os usuarios em ordem alfabética apenas com as informações: nome, email e a empresa em que trabalha

**http://localhost:8080/usuarios/suite** - Traz os usuários que no endereço contém a palavra 'suite'

Obs.: Caso a rota não exista, você irá se deparar com erro o 404.

> ## Testes

Para executar os testes os testes unitários, execute o comando **npm test** dentro da pasta do projeto

> ## Tecnologias utilizadas

* Mocha - Para mostrar o relatório de testes
* Chai - Para realizar os testes unitários
* Express-async-handler - Para utilizar async e await nas rotas
* Express - Para facilitar na criação de rotas/middlewares
* Winston - Para salvar os logs de todas as interações


> ## Observação

* Ao invés de utilizar o elasticsearch para realizar os logs, utilizei o winston, pois não consegui criar uma VM para poder executar o elasticsearch.
